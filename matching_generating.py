#!/usr/bin/python
"""
This code is part of the Arc-flow Vector Packing Solver (VPSolver).

Copyright (C) 2013-2015, Filipe Brandao
Faculdade de Ciencias, Universidade do Porto
Porto, Portugal. All rights reserved. E-mail: <fdabrandao@dcc.fc.up.pt>.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import sys, os

sdir = os.path.dirname(__file__)
if sdir != "": os.chdir(sdir)

""" Add VPSolver folders to path """

# add vpsolver folder to sys.path
sys.path.insert(0, "../")

# add vpsolver/bin folder to path
os.environ["PATH"] = "../bin" + ":" + os.environ["PATH"]

# add vpsolver/scripts folder to path
os.environ["PATH"] = "../scripts" + ":" + os.environ["PATH"]

""" Example """

## load all the vpsolver utils ##
from pyvpsolver import *

ratio_list = [1, 3, 5, 7, 6, 5, 10, 8, 4, 3]  # the ratio of different VM type

for i in range(1, 200):
    print i, "-----------------------"
    tmp_ratio_list = []
    for j in range(0, 10):
        tmp_ratio_list.append(ratio_list[j] * i)
    print(tmp_ratio_list)

    ## Creating instanceA ##
    instance = VBP([20, 20, 20],
                   [[7, 6, 1], [2, 4, 4], [7, 1, 9], [2, 4, 8], [1, 4, 7], [7, 6, 5], [3, 8, 9], [5, 2, 7], [10, 2, 5],
                    [4, 6, 10]], tmp_ratio_list, verbose=True)

    ## Creating instance from a .vbp file ##
    # instance = VBP.fromFile("vector_packing_test.vbp", verbose=False)

    # solving an instance directly without creating AFG, MPS or LP objects
    out, sol = VPSolver.script("vpsolver_glpk.sh", instance, verbose=False)

    ## printing the solution ##
    obj, patterns = sol
    # print "Objective:", obj
    # print "Patterns:", patterns
    #
    # for item in patterns:
    # print "item: ", item
    #
    # print "Solution:", sol

    ## pretty print for solutions ##
    print_solution_vbp(obj, patterns)
    print("")

