#!/usr/bin/python
"""
This code is part of the Arc-flow Vector Packing Solver (VPSolver).

Copyright (C) 2013-2015, Filipe Brandao
Faculdade de Ciencias, Universidade do Porto
Porto, Portugal. All rights reserved. E-mail: <fdabrandao@dcc.fc.up.pt>.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import sys, os

sdir = os.path.dirname(__file__)
if sdir != "": os.chdir(sdir)

""" Add VPSolver folders to path """

# add vpsolver folder to sys.path
sys.path.insert(0, "../")

# add vpsolver/bin folder to path
os.environ["PATH"] = "../bin" + ":" + os.environ["PATH"]

# add vpsolver/scripts folder to path
os.environ["PATH"] = "../scripts" + ":" + os.environ["PATH"]

""" Example """

## load all the vpsolver utils ##
from pyvpsolver import *
import random

# ratio_list = [1, 3, 5, 7, 6, 5, 10, 8, 4, 3]  # the ratio of different VM type
# ratio_list = [1, 3, 5, 1, 3, 5, 2, 5, 2, 5]  # the ratio of different VM type
capacity_list = [20, 20, 20]
item_type_list = [[7, 6, 1], [2, 4, 4], [7, 1, 9], [2, 4, 8], [1, 4, 7], [7, 6, 5], [3, 8, 9], [5, 2, 7], [10, 2, 5],
                  [4, 6, 10]]
# item_type_list = [[12, 6, 3], [2, 4, 13], [7, 5, 15], [8, 14, 17], [18, 16, 7], [13, 15, 5], [3, 16, 9], [16, 14, 9],
#                   [17, 4, 11], [4, 6, 11]]

ratio_list = []
for i in range(1, 11):
    ratio_list.append(random.randint(1, 10))
print('ratio_list: ' + str(ratio_list))

# item_type_list = []
# for i in range(1, 11):
#     type = []
#     for i in range(1, 4):
#         type.append(random.randint(1, 10))
#     item_type_list.append(type)

print('item_type_list: ' + str(item_type_list))

type_set = set()
item_sum_dict = {}
item_sum_ratio_dict = {}
item_sum = 0
type_ratio_dict = {}
resource_utilization_list1 = []
resource_utilization_list2 = []
resource_utilization_list3 = []

for i in range(10, 211):
    print i, "-----------------------"
    tmp_ratio_list = []
    for j in range(0, 10):
        tmp_ratio_list.append(ratio_list[j] * i)
    print(tmp_ratio_list)

    ## Creating instanceA ##
    instance = VBP(capacity_list, item_type_list, tmp_ratio_list, verbose=False)

    ## Creating instance from a .vbp file ##
    # instance = VBP.fromFile("vector_packing_test.vbp", verbose=False)

    # solving an instance directly without creating AFG, MPS or LP objects
    out, sol = VPSolver.script("vpsolver_glpk.sh", instance, verbose=False)

    ## printing the solution ##
    obj, patterns = sol
    # print "Objective:", obj
    # print "Patterns:", patterns

    item_sum = item_sum + obj

    d1 = 0
    d2 = 0
    d3 = 0
    for item in patterns:
        counter_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        # print "item: ", item
        usage1 = 0
        usage2 = 0
        usage3 = 0
        for val in item[1]:
            counter_list[val] = counter_list[val] + 1
            usage1 = usage1 + item_type_list[val][0]
            usage2 = usage2 + item_type_list[val][1]
            usage3 = usage3 + item_type_list[val][2]
        type_set.add(str(counter_list))

        # recording the resource usages
        d1 = d1 + item[0] * usage1
        d2 = d2 + item[0] * usage2
        d3 = d3 + item[0] * usage3

        # recording the type ratio
        # print(str(counter_list) + ': ' + str(round(item[0] / (obj * 1.0), 4)))
        if str(counter_list) in type_ratio_dict:
            tmplist = type_ratio_dict[str(counter_list)]
            tmplist.append(round(item[0] / (obj * 1.0), 4))
        else:
            type_ratio_list = []
            type_ratio_list.append(round(item[0] / (obj * 1.0), 4))
            type_ratio_dict[str(counter_list)] = type_ratio_list

        if str(counter_list) in item_sum_dict:
            item_sum_dict[str(counter_list)] = item_sum_dict[str(counter_list)] + item[0]
        else:
            item_sum_dict[str(counter_list)] = item[0]



    # recording the resource usages
    resource_utilization_list1.append(round(d1 / (obj * capacity_list[0] * 1.0), 3))
    resource_utilization_list2.append(round(d2 / (obj * capacity_list[1] * 1.0), 3))
    resource_utilization_list3.append(round(d3 / (obj * capacity_list[2] * 1.0), 3))
    ## pretty print for solutions ##
    print_solution_vbp(obj, patterns)
    print("")

print('type set--------------------')
for type_item in type_set:
    print('type item: ' + type_item)

print('item_sum_dict------------------')
for item in item_sum_dict:
    print(item + ': ' + str(item_sum_dict[item]))
    item_sum_ratio_dict[item] = round(item_sum_dict[item] / (item_sum * 1.0), 3)

print('item_sum: ' + str(item_sum))

print('item_sum_ratio_dict----------------')
for item in item_sum_ratio_dict:
    print(item + ': ' + str(item_sum_ratio_dict[item]))

print("type ratio dict----------------")
for dict_item in type_ratio_dict:
    print(dict_item + ': ' + str(type_ratio_dict[dict_item]))

print("resource utilization--------------")
print("resource_utilization_list1: " + str(resource_utilization_list1))
print("resource_utilization_list2: " + str(resource_utilization_list2))
print("resource_utilization_list3: " + str(resource_utilization_list3))
